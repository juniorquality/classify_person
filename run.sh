#!/bin/bash

if [ "$(id -u)" != "0" ]; then
  echo "Sorry, you are not root."
  exit 1
fi

NOW=$(date +"%d-%m-%Y")

FULLPATH=${PWD}

NAME="Reconhecimento de imagens"
PHP_BIN_DIR=""
PHP_PATH=""


APPLICATION_PATH="${PWD}/shells/"

PIDFILE_RECONHECIMENTO_NOHUP="/var/run/reconhecimento.pid"
PIDATUAL_RECONHECIMENTO_NOHUP="";
PIDATUAL_RECONHECIMENTO_SH="";

PIDFILE_CONTAGEM_NOHUP="/var/run/contagem.pid"
PIDATUAL_CONTAGEM_NOHUP="";
PIDATUAL_CONTAGEM_SH="";

PIDFILE_VEICULO_NOHUP="/var/run/veiculo.pid"
PIDATUAL_VEICULO_NOHUP="";
PIDATUAL_VEICULO_SH="";

start() {
  PIDATUAL_RECONHECIMENTO_NOHUP=$(cat $PIDFILE_RECONHECIMENTO_NOHUP);
  PIDATUAL_RECONHECIMENTO_SH=$(ps -aux | grep ${FULLPATH}/detectionPeople.py | grep -v grep | awk '{print $2}');

  if [ -f /proc/$PIDATUAL_RECONHECIMENTO_NOHUP/exe ]
  then
    echo "PROCESSO DE RECONHECIMENTO FOI INICIALIZADO"
    echo "PID: $PIDATUAL_RECONHECIMENTO_NOHUP - $PIDATUAL_RECONHECIMENTO_SH";
  else
    echo "INICIANDO PROCESSO DE RECONHECIMENTO";
    nohup sh ${APPLICATION_PATH}reconhecimento.sh & echo $! > $PIDFILE_RECONHECIMENTO_NOHUP
    PIDATUAL_RECONHECIMENTO_NOHUP=$(cat $PIDFILE_RECONHECIMENTO_NOHUP);
    echo "PID: $PIDATUAL_RECONHECIMENTO_NOHUP";
  fi

  # PIDATUAL_CONTAGEM_NOHUP=$(cat $PIDFILE_CONTAGEM_NOHUP);
  # if [ -f /proc/$PIDATUAL_CONTAGEM_NOHUP/exe ]
  # then
  #   echo "PROCESSO DE CONTAGEM FOI INICIALIZADO"
  #   echo "PID: $PIDATUAL_CONTAGEM_NOHUP";
  # else
  #   echo "INICIANDO PROCESSO DE CONTAGEM";
  #   nohup sh ${APPLICATION_PATH}contagem.sh & echo $! > $PIDFILE_CONTAGEM_NOHUP
  #   PIDATUAL_CONTAGEM_NOHUP=$(cat $PIDFILE_CONTAGEM_NOHUP);
  #   echo "PID: $PIDATUAL_CONTAGEM_NOHUP";
  # fi

  # PIDATUAL_VEICULO_NOHUP=$(cat $PIDFILE_VEICULO_NOHUP);
  # if [ -f /proc/$PIDATUAL_VEICULO_NOHUP/exe ]
  # then
  #   echo "PROCESSO DE VEICULO FOI INICIALIZADO"
  #   echo "PID: $PIDATUAL_VEICULO_NOHUP";
  # else
  #   echo "INICIANDO PROCESSO DE VEICULO";
  #   nohup sh ${APPLICATION_PATH}veiculo.sh & echo $! > $PIDFILE_VEICULO_NOHUP
  #   PIDATUAL_VEICULO_NOHUP=$(cat $PIDFILE_VEICULO_NOHUP);
  #   echo "PID: $PIDATUAL_VEICULO_NOHUP";
  # fi
  RETVAL=$?;
}

stop() {
  echo "Finalizando servico ${NAME}";
  PIDATUAL_RECONHECIMENTO_NOHUP=$(cat $PIDFILE_RECONHECIMENTO_NOHUP);
  PIDATUAL_RECONHECIMENTO_SH=$(ps -aux | grep ${FULLPATH}/detectionPeople.py | grep -v grep | awk '{print $2}');
  echo "PIDATUAL_RECONHECIMENTO_SH: $PIDATUAL_RECONHECIMENTO_SH"

  if [ -f /proc/$PIDATUAL_RECONHECIMENTO_NOHUP/exe ]
  then
    echo "PID: $PIDATUAL_RECONHECIMENTO_NOHUP";
    kill -9 $PIDATUAL_RECONHECIMENTO_NOHUP;
    kill -9 $PIDATUAL_RECONHECIMENTO_SH;
    echo "-1245" > $PIDFILE_RECONHECIMENTO_NOHUP;
    RETVAL=$?
  else
    echo "PROCESSO DE RECONHECIMENTO FOI FINALIZADO";
  fi

  # PIDATUAL_CONTAGEM_NOHUP=$(cat $PIDFILE_CONTAGEM_NOHUP);
  # PIDATUAL_CONTAGEM_SH=$(ps -aux | grep 'python ${FULLPATH}/contagem.py' | grep -v grep | awk '{print $2}');
  # echo "PIDATUAL_CONTAGEM_SH: $PIDATUAL_CONTAGEM_SH"

  # if [ -f /proc/$PIDATUAL_CONTAGEM_NOHUP/exe ]
  # then
  #   echo "PID: $PIDATUAL_CONTAGEM_NOHUP";
  #   kill -9 $PIDATUAL_CONTAGEM_NOHUP;
  #   kill -9 $PIDATUAL_CONTAGEM_SH;
  #   echo "-1245" > $PIDFILE_CONTAGEM_NOHUP;
  #   RETVAL=$?
  # else
  #   echo "PROCESSO DE CONTAGEM FOI FINALIZADO";
  # fi

  # PIDATUAL_VEICULO_NOHUP=$(cat $PIDFILE_VEICULO_NOHUP);
  # PIDATUAL_VEICULO_SH=$(ps -aux | grep 'python ${FULLPATH}/veiculo.py' | grep -v grep | awk '{print $2}');
  # echo "PIDATUAL_VEICULO_SH: $PIDATUAL_VEICULO_SH"

  # if [ -f /proc/$PIDATUAL_VEICULO_NOHUP/exe ]
  # then
  #   echo "PID: $PIDATUAL_VEICULO_NOHUP";
  #   kill -9 $PIDATUAL_VEICULO_NOHUP;
  #   kill -9 $PIDATUAL_VEICULO_SH;
  #   echo "-1245" > $PIDFILE_VEICULO_NOHUP;
  #   RETVAL=$?
  # else
  #   echo "PROCESSO DE VEICULO FOI FINALIZADO";
  # fi
}

restart() {
    stop;
    start;
}

status() {
  PIDATUAL_RECONHECIMENTO_NOHUP=$(cat $PIDFILE_RECONHECIMENTO_NOHUP);
  PIDATUAL_RECONHECIMENTO_SH=$(ps -aux | grep ${FULLPATH}/detectionPeople.py | grep -v grep | awk '{print $2}');
  if [ -f /proc/$PIDATUAL_RECONHECIMENTO_NOHUP/exe ]
  then
    echo "PROCESSO DE RECONHECIMENTO ESTA RODANDO NORMALMENTE"
    echo "PID: $PIDATUAL_RECONHECIMENTO_NOHUP - $PIDATUAL_RECONHECIMENTO_SH";
  else
    echo "Servico ${NAME} foi finalizado";
  fi

  # PIDATUAL_CONTAGEM_NOHUP=$(cat $PIDFILE_CONTAGEM_NOHUP);
  # PIDATUAL_CONTAGEM_SH=$(ps -aux | grep 'python ${FULLPATH}/contagem.py' | grep -v grep | awk '{print $2}');
  # #echo "PIDATUAL_CONTAGEM_SH: $PIDATUAL_CONTAGEM_SH"
  # if [ -f /proc/$PIDATUAL_CONTAGEM_NOHUP/exe ]
  # then
  #   echo "PROCESSO DE CONTAGEM ESTA RODANDO NORMALMENTE"
  #   echo "PID: $PIDATUAL_CONTAGEM_NOHUP - $PIDATUAL_CONTAGEM_SH";
  # else
  #   echo "Servico ${NAME} foi finalizado";
  # fi

  # PIDATUAL_VEICULO_NOHUP=$(cat $PIDFILE_VEICULO_NOHUP);
  # PIDATUAL_VEICULO_SH=$(ps -aux | grep 'python ${FULLPATH}/veiculo.py' | grep -v grep | awk '{print $2}');
  # if [ -f /proc/$PIDATUAL_VEICULO_NOHUP/exe ]
  # then
  #   echo "PROCESSO DE VEICULO ESTA RODANDO NORMALMENTE"
  #   echo "PID: $PIDATUAL_VEICULO_NOHUP - $PIDATUAL_VEICULO_SH";
  # else
  #   echo "Servico ${NAME} foi finalizado";
  # fi
}

case "$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    status)
        status
        ;;
    restart)
        restart
        ;;
    *)
        restart
        ;;
esac
exit $RETVAL