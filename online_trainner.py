import base64
import os
import time
# import the necessary packages
from io import BytesIO

import cv2
import numpy as np
import pymysql as MySQLdb
from PIL import Image

con = MySQLdb.connect(
    host='192.168.1.51', port=3306,
    user='quality', passwd='LGboavCWgtzC', db='quality')


base_dir = 'train_filters/pessoas/'


def update_with_date(cur, list_ids):
    chunks = [list_ids[x:x + 900] for x in xrange(0, len(list_ids), 900)]
    for to_update in chunks:
        sql = "update quality.qualidade_imagem_filtrada set utilizado=NOW() where id IN({})".format(
            ",".join(str(x) for x in to_update))
        print(sql)
        cur.execute(sql)


def create_folder_person(dir_rel, limit):
    os.system('mkdir -p {}acesso_pessoas'.format(dir_rel))
    cur = con.cursor(MySQLdb.cursors.DictCursor)
    sql = 'SELECT * FROM quality.qualidade_imagem_filtrada F where F.status_json is not null and F.status_json like \'%\"teia_aranha\":false%\'  and F.status_json like \'%\"interferencia\":false%\'  and F.status_json like \'%\"sem_imagem\":false%\'  and(F.status_json like \'%\"pessoa\":true%\' or  F.status_json like \'%\"pessoa_capacete\":true%\' or  F.status_json like \'%\"pessoa_bone\":true%\' or  F.status_json like \'%\"pessoa_touca\":true%\'  or  F.status_json like \'%\"pessoa_uniforme\":true%\' or  F.status_json like \'%\"pessoa_dormindo\":true%\' or  F.status_json like \'%\"pessoa_epi\":true%\')  order by utilizado limit  {}'.format(
        limit)
    print(sql)
    cur.execute(sql)
    query = cur.fetchall()
    # print(query)

    to_update = []

    for j, i in enumerate(query):
        try:
            # print(i['status_json'])
            pimg = Image.open(BytesIO(base64.b64decode(i['foto'])))
            pimg = np.array(pimg)
            cv2.imwrite(dir_rel + 'acesso_pessoas/people{:06d}.jpg'.format(
                int(time.time() * 1000)), pimg[:, :, ::-1])

            to_update.append(i['id'])
            # sql = "update quality.qualidade_imagem_filtrada set utilizado=NOW() where id={}".format(i['id'])

            # print(sql)
            # cur.execute(sql)
            # con.commit()

        except Exception as e:
            sql = "delete from quality.qualidade_imagem_filtrada where id={}".format(
                i['id'])
            print(sql)
            cur.execute(sql)
            con.commit()

            print('Error id :{}, {}'.format(i['id'], e))
            continue

    update_with_date(cur, to_update)
    con.commit()


def create_folder_noperson(dir_rel, limit):
    os.system('mkdir -p {}sem_pessoas'.format(dir_rel))
    cur = con.cursor(MySQLdb.cursors.DictCursor)
    sql = 'SELECT * FROM quality.qualidade_imagem_filtrada F where F.status_json is not null and F.status_json like \'%\"pessoa\":false,\"pessoa_capacete\":false,\"pessoa_bone\":false,\"pessoa_touca\":false\
,\"pessoa_uniforme\":false,\"pessoa_dormindo\":false,\"pessoa_epi\":false%\' and F.status_json like \'%\"teia_aranha\":false%\' and F.status_json like \'%\"interferencia\":false%\'  and F.status_json like \'%\"sem_imagem\":false%\' and F.status_json like \'%\"porta_aberta\":false%\'    order by utilizado limit {}'.format(limit)
    print(sql)
    cur.execute(sql)
    query = cur.fetchall()
    # print(query)
    to_update = []
    for j, i in enumerate(query):
        try:
            # print(i['status_json'])
            pimg = Image.open(BytesIO(base64.b64decode(i['foto'])))
            pimg = np.array(pimg)
            # cv2.imshow('image', pimg[:, :, ::-1])
            # cv2.waitKey(0)
            cv2.imwrite(dir_rel + 'sem_pessoas/no_people{:06d}.jpg'.format(
                int(time.time() * 1000)), pimg[:, :, ::-1])
            to_update.append(i['id'])
            # sql = "update quality.qualidade_imagem_filtrada set utilizado=1 where id={}".format(i['id'])
            # sql = "update quality.qualidade_imagem_filtrada set utilizado=NOW() where id={}".format(i['id'])
            # print(sql)
            # cur.execute(sql)
            # con.commit()

        except Exception as e:
            sql = "delete from quality.qualidade_imagem_filtrada where id={}".format(
                i['id'])
            print(sql)
            cur.execute(sql)
            con.commit()

            print('Error id :{}, {}'.format(i['id'], e))
            continue
    update_with_date(cur, to_update)
    con.commit()


def create_folder_noise2person(dir_rel, limit):
    os.system('mkdir -p {}erro_interferencia'.format(dir_rel))
    cur = con.cursor(MySQLdb.cursors.DictCursor)
    sql = 'SELECT * FROM quality.qualidade_imagem_filtrada F where F.status_json is not null and F.status_json like \'%\"pessoa\":false%\' and F.status_json like \'%\"teia_aranha\":false%\' and (F.status_json like \'%\"interferencia\":true%\' or F.status_json like \'%\"sem_imagem\":true%\') order by utilizado limit {}'.format(
        limit)
    print(sql)
    cur.execute(sql)
    query = cur.fetchall()
    # print(query)
    to_update = []
    for j, i in enumerate(query):
        try:
            # print(i['status_json'])
            pimg = Image.open(BytesIO(base64.b64decode(i['foto'])))
            pimg = np.array(pimg)
            cv2.imwrite(dir_rel + 'erro_interferencia/noise{:06d}.jpg'.format(
                int(time.time() * 1000)), pimg[:, :, ::-1])
            to_update.append(i['id'])
            # sql = "update quality.qualidade_imagem_filtrada set utilizado=1 where id={}".format(i['id'])
            # sql = "update quality.qualidade_imagem_filtrada set utilizado=NOW() where id={}".format(i['id'])
            # print(sql)
            # cur.execute(sql)
            # con.commit()

        except Exception as e:
            sql = "delete from quality.qualidade_imagem_filtrada where id={}".format(
                i['id'])
            print(sql)
            cur.execute(sql)
            con.commit()
            print('Error id :{}, {}'.format(i['id'], e))
            continue
    update_with_date(cur, to_update)
    con.commit()


def create_folder_spyder2person(dir_rel, limit):
    os.system('mkdir -p {}erro_interferencia'.format(dir_rel))
    cur = con.cursor(MySQLdb.cursors.DictCursor)
    sql = 'SELECT * FROM quality.qualidade_imagem_filtrada F where F.status_json is not null and F.status_json like \'%\"pessoa":false%\' and F.status_json like \'%\"teia_aranha":true%\'  order by utilizado limit {}'.format(
        limit)
    print(sql)
    cur.execute(sql)
    query = cur.fetchall()
    # print(query)
    to_update = []
    for j, i in enumerate(query):
        try:
            # print(i['status_json'])
            pimg = Image.open(BytesIO(base64.b64decode(i['foto'])))
            pimg = np.array(pimg)
            cv2.imwrite(dir_rel + 'erro_interferencia/spyder{:06d}.jpg'.format(
                int(time.time() * 1000)), pimg[:, :, ::-1])
            to_update.append(i['id'])
            # sql = "update quality.qualidade_imagem_filtrada set utilizado=1 where id={}".format(i['id'])
            # sql = "update quality.qualidade_imagem_filtrada set utilizado=NOW() where id={}".format(i['id'])
            # print(sql)
            # cur.execute(sql)
            # con.commit()
        except Exception as e:
            sql = "delete from quality.qualidade_imagem_filtrada where id={}".format(
                i['id'])
            print(sql)
            cur.execute(sql)
            con.commit()
            print('Error id :{}, {}'.format(i['id'], e))
            continue
    update_with_date(cur, to_update)
    con.commit()


def make_dataset(tam=3000):
    os.system('rm {} -r'.format(base_dir))
    create_folder_spyder2person(base_dir + '/train/', tam // 2)
    create_folder_noise2person(base_dir + '/train/', tam // 2)
    create_folder_noperson(base_dir + '/train/', tam)
    create_folder_person(base_dir + '/train/', tam)

    create_folder_spyder2person(base_dir + '/val/', (tam // 10) // 2)
    create_folder_noise2person(base_dir + '/val/', (tam // 10) // 2)
    create_folder_noperson(base_dir + '/val/', (tam // 10))
    create_folder_person(base_dir + '/val/', (tam // 10))


if __name__ == "__main__":
    make_dataset(100)
