#!/usr/bin/env python
import base64
import json
import sys
import threading
import time
from collections import defaultdict
from StringIO import StringIO
from flask import Flask
from PIL import Image
from datetime import datetime, timedelta

import numpy as np

import scipy.misc
from keras import backend as K
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.optimizers import Adagrad
from keras.preprocessing.image import ImageDataGenerator
from keras.utils import np_utils

import dataset
import net

import math
import cv2
import traceback
import os
import glob
import uuid
import MySQLdb
import requests

dadosDB = json.load(open('settings.json'))['database']['cloudsight']

os.environ['TZ'] = 'America/Sao_Paulo'
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import tensorflow as tf
from keras.backend.tensorflow_backend import set_session
config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.1
set_session(tf.Session(config=config))

app = Flask(__name__)
try:
    db = MySQLdb.connect(host=dadosDB['host'], port=dadosDB['port'], user=dadosDB['username'], passwd=dadosDB['password'], db=dadosDB['database'])
except Exception as e:
    print("ERRORRRR AO CONECTAR")
    exit()

cursor = db.cursor()


model_ = None
tags_from_model = None

lock = threading.Lock()

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def resize_img(img_to_resize, area=1024 * 768):
    """
    Resize a image keep de ratio and return a factor scale.
    Args:
        img_to_resize: opencv Mat/nmupy to resize.
        area: Target area to resize
    Returns:
        A Tuple with image an factor of scale
    """
    factor = float(img_to_resize.shape[1]) / float(img_to_resize.shape[0])
    new_h = int(math.sqrt((area) / factor) + 0.5)
    new_w = int((new_h * factor) + 0.5)

    img_resized = cv2.resize(img_to_resize, (new_w, new_h))
    return img_resized, img_to_resize.shape[1] / float(img_resized.shape[1])


def readb64(base64_string):
    sbuf = StringIO()
    sbuf.write(base64.b64decode(base64_string))
    pimg = Image.open(sbuf)
    return np.array(pimg, dtype=np.uint8)


def validate(model, X_test, y_test, batch_size):
    loss = model.evaluate(X_test, y_test, batch_size=batch_size)
    Y_pred = model.predict(X_test, batch_size=batch_size)
    print "Loss {}, Accuracy: {}".format(loss[0], loss[1])


def train():
    global tags_from_model, lock

    model, tags_from_model = net.load("model")

    for layer in model.layers[:172]:
        layer.trainable = False
    for layer in model.layers[172:]:
        layer.trainable = True
    model.summary()

    app.config.update({"MODEL": model})
    # we need to recompile the model for these modifications to take effect
    # we use SGD with a low learning rate

    app.config["MODEL"].compile(optimizer=Adagrad(lr=0.0001),
                                loss='categorical_crossentropy',
                                metrics=["accuracy"])

def analiseImage(image64):
    global tags_from_model
    img = readb64(image64)
    resizeim = cv2.resize(img, (224, 224))
    dims = resizeim.shape
    resizeim = dataset.preprocess_input(
        resizeim).reshape(-1, dims[0], dims[1], 3)
    with lock:
        Y_pred = app.config["MODEL"].predict(resizeim, batch_size=1)

    ret = {}
    for i, t in enumerate(tags_from_model):
        ret[t] = float(Y_pred[0, i])

    return ret

def saveImage(frame, response):
    cv2.putText(frame, "ACESSO: {}".format(int(response['acesso_pessoas'] * 100)), (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
    cv2.putText(frame, "ERROR: {}".format(int(response['erro_interferencia'] * 100)), (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
    cv2.putText(frame, "SEM: {}".format(int(response['sem_pessoas'] * 100)), (10, 100), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
    hashID = uuid.uuid4()
    newName = os.path.sep.join(
        [
            'image-analise',
            "{}.jpg" . format( hashID )
        ]
    )
    cv2.imwrite(newName,frame)
    return newName;

def contagemPessoas(imagem):
    try:
      url = 'http://192.168.1.157:10000/counting_people'
      files = {'image': open(imagem, 'rb')}
      response = requests.post(url, files=files)
      jsonResponse = response.json()
    except Exception as e:
        print(e)
        jsonResponse = {
            'people_num':0
        }
    return jsonResponse

def createImage(imgstring, id):
  fh = open("./imagem-reconhecimento.jpeg", "wb")
  fh.write(bytearray(imgstring))
  fh.close()
  return './imagem-reconhecimento.jpeg'

def updateImagem(id, resultado, veiculo, contagem):
  acessoPessoas = resultado["acesso_pessoas"] * 100

  dadosUpdate = {
    'acesso pessoas':0,
    'sem pessoas':0,
    'error':0,
    'mais de uma':0
  };
  dadosUpdate["acesso pessoas"] = resultado["acesso_pessoas"]
  dadosUpdate["sem pessoas"] = resultado["sem_pessoas"]
  dadosUpdate["error"] = resultado["erro_interferencia"]

  dadosUpdate["mais de uma"] = 0
  if contagem["people_num"] > 1:
    dadosUpdate["mais de uma"] = 1

  dadosUpdate["sem veiculos"] = 0
  dadosUpdate["acesso veiculos"] = 0
  dadosUpdate["quantidade_pessoas"] = contagem["people_num"]

  sUpdate = (' UPDATE imagem SET resultado = "{}", sistema = 1, dh_processamento = NOW()  WHERE id = "{}";').format(dadosUpdate, id)
  cursor = db.cursor()
  cursor.execute(sUpdate)
  db.commit()
    
if __name__ == "__main__":
    train();

    text = '';
    try:
        i = 0
        loop = True;
        while loop:
            i = i+1;
            dateNow = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            dateOneHourPast = (datetime.today() - timedelta(hours=1)).strftime("%Y-%m-%d %H:%M:%S")
            sqlSelect = ("SELECT * FROM imagem WHERE dh_registro BETWEEN '{}' AND '{}' AND sistema = 0 AND resultado = ''").format(dateOneHourPast, dateNow)
            #sqlSelect = ("SELECT * FROM cloudsight.imagem ORDER BY 1 DESC LIMIT 10")
            print(sqlSelect)
            cursor.execute(sqlSelect)
            db.commit()
            print(cursor.rowcount)
            if(cursor.rowcount > 0):
                text = ''
                data = cursor.fetchall()
                for row in data :
                    imgS = base64.encodestring(row[2])
                    imagem = createImage(row[2], row[0])

                    classificacao = analiseImage(imgS)
                    jsonResponse = contagemPessoas(imagem)
                    print(classificacao, jsonResponse)

                    time_delta = time.time()
                    updateImagem(row[0], classificacao, row[13], jsonResponse)

            else:
                time.sleep(1)

            if(text != 'SEM LINHAS'):
                text = 'SEM LINHAS'
                print(text)

    except KeyboardInterrupt:
        db.close()
        print("\nACABOUUUU ")
