#!/bin/bash

export MY_WORKER_PID_R=0           # PID do worker (python)
export MY_MANAGER_PID_R=$$       # PID do manager (shellscript)

run_worker()
{
    FULLPATH=${PWD}
    python "${FULLPATH}/detectionPeople.py" &
    export MY_WORKER_PID_R=$!
}

while true; do
    if [ "$MY_WORKER_PID_R" -eq "0" ]; then
        run_worker
    fi;

    if ! kill -0 $MY_WORKER_PID_R > /dev/null 2>&1; then
        run_worker
    fi;
    sleep 5
done    